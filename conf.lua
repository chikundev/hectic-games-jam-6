-- chikun :: 2015
-- The configuration file for our game


function love.conf(game)

    game.window.width   = 854
    game.window.height  = 480
    game.window.resizable = true
    game.window.title   = "Cooking Llama"
    game.window.vsync   = false
    game.window.icon    = "gfx/icon.png"

    -- Set to false before distribution.
    game.console = true

end

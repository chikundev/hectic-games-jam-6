return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.11.0",
  orientation = "orthogonal",
  width = 96,
  height = 54,
  tilewidth = 20,
  tileheight = 20,
  nextobjectid = 16,
  properties = {},
  tilesets = {
    {
      name = "bg01",
      firstgid = 1,
      tilewidth = 1920,
      tileheight = 1080,
      spacing = 0,
      margin = 0,
      image = "../gfx/bg01.png",
      imagewidth = 1920,
      imageheight = 1080,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "bg01Oven",
      firstgid = 2,
      tilewidth = 391,
      tileheight = 209,
      spacing = 0,
      margin = 0,
      image = "../gfx/bg01Oven.png",
      imagewidth = 391,
      imageheight = 209,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "bg01Stove",
      firstgid = 3,
      tilewidth = 390,
      tileheight = 207,
      spacing = 0,
      margin = 0,
      image = "../gfx/bg01Stove.png",
      imagewidth = 390,
      imageheight = 207,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "bg01Board",
      firstgid = 4,
      tilewidth = 394,
      tileheight = 211,
      spacing = 0,
      margin = 0,
      image = "../gfx/bg01Board.png",
      imagewidth = 394,
      imageheight = 211,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "bg01Sink",
      firstgid = 5,
      tilewidth = 395,
      tileheight = 256,
      spacing = 0,
      margin = 0,
      image = "../gfx/bg01Sink.png",
      imagewidth = 395,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "objectgroup",
      name = "background",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 3,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 1080,
          width = 1920,
          height = 1080,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "workspaces",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 7,
          name = "stove",
          type = "",
          shape = "rectangle",
          x = 1535,
          y = 870,
          width = 390,
          height = 207,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {}
        },
        {
          id = 5,
          name = "oven",
          type = "",
          shape = "rectangle",
          x = 1535,
          y = 1080,
          width = 391,
          height = 209,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          id = 10,
          name = "board",
          type = "",
          shape = "rectangle",
          x = 1140,
          y = 1080,
          width = 394,
          height = 211,
          rotation = 0,
          gid = 4,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "sink",
          type = "",
          shape = "rectangle",
          x = 1135,
          y = 870,
          width = 395,
          height = 256,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {}
        }
      }
    }
  }
}

-- chikun :: 2015
-- Hectic Games Jam #6



--[[
    Here we will load the chikun specific libaries
  ]]
require "lib/bindings"      -- Very important that you read this file

-- Load particle functions
require "lib/iGrid"

-- Set default scaling
g.setDefaultFilter('linear')

require "lib/maths"         -- Load maths functions
require "lib/misc"          -- Load miscellaneous functions

--[[
    The recursive loading functions pretty much convert / to . and
    make file extensions void.
    eg. gfx/player.png becomes gfx.player,
        sfx/enemies/die becomes sfx.enemies.die
  ]]

-- Game canvas
gameCanvas = g.newCanvas(1920, 1080)
mgCanvas = g.newCanvas(1920, 1080)

require "lib/load/fonts"    -- Load fonts into fnt table [not recursive]
require "lib/load/gfx"      -- Load png graphics from gfx folder to gfx table
require "lib/load/maps"     -- Load all maps from map folder to maps table
require "lib/load/snd"      -- Load ogg sounds from bgm and sfx to bgm and sfx
require "lib/load/states"   -- Load states from src/states to states table

require "lib/stateManager"  -- Load state manager
require "lib/mapTool"       -- Load mapTool :: don't try reading this

require "src/lastInput"


-- Performed at game startup
function love.load()


    -- Set current state to play state
    state.load(states.select)

    gameSpeed = 1

    -- Has mouse clicked?
    clicked = false

    vol = {
        bgm = 1,
        sfx = 1
    }

    feedback = {
        timer = 0,
        type = "no"
    }

    -- How many recipes player has unlocked
    progress = 3

    recipes = {
        require "recipes/baconAndEggs",
        require "recipes/burger",
        require "recipes/souffle"
    }

    lastInput = "mouse"


end



-- Performed on game update
function love.update(dt)

    feedback.timer = math.max(feedback.timer - dt * 2, 0)

    local joysticks = love.joystick.getJoysticks( )

    -- Find scale values
    local scaleW, scaleH =
        w.getWidth() / 1920,
        w.getHeight() / 1080

    local scaleVal = math.min(scaleW, scaleH)

    for i, joystick in ipairs(joysticks) do

        local xAxis, yAxis = 0, 0

        if joystick:isGamepad() then

            xAxis = joystick:getGamepadAxis('leftx')
            yAxis = joystick:getGamepadAxis('lefty')

            unclick = joystick:isGamepadDown('a')

        else

            xAxis = joystick:getAxis(1)
            yAxis = joystick:getAxis(2)

            unclick = joystick:isDown(1)

        end

        if math.abs(xAxis) > 0.2 then

            m.setX(m.getX() + xAxis * dt * 1000 * scaleVal)

        end

        if math.abs(yAxis) > 0.2 then

            m.setY(m.getY() + yAxis * dt * 1000 * scaleVal)

        end
    end

    if lastInput == "mouse" then

        unclick = m.isDown('l')

    end

    --[[
        Limit dt to 1/15 and induce lag if below 15 FPS.
        This is to save calculations.
      ]]
    dt = math.min(dt, 1/15)

    dt = dt * gameSpeed

    hasClicked, hasReleased =
        false,
        false

    if clicked and not unclick then

        hasReleased = true

    end

    if not clicked and unclick then

        hasClicked = true

    end

    clicked = unclick

    -- Find scale values
    local scaleW, scaleH =
        w.getWidth() / 1920,
        w.getHeight() / 1080

    local scaleVal = math.min(scaleW, scaleH)

    -- Update the cursor
    curs = {
        x = (m.getX() / scaleVal) - (math.max(scaleW / scaleH, 1) - 1) * 960,
        y = (m.getY() / scaleVal) - (math.max(scaleH / scaleW, 1) - 1) * 540,
        w = 1, h = 1
    }

    -- Update current state
    state.current:update(dt)

    for key, val in pairs(bgm) do

        val:setVolume(vol.bgm)

    end

    for key, val in pairs(sfx) do

        val:setVolume(vol.sfx)

    end

    if tolerance and countdown == 0 then

        tolerance = math.max(tolerance - dt * 2, 0)

        if tolerance == 0 then

            state.change(states.lose)

        end

        timer = math.max(timer - dt, 0)
    end
end



-- Performed on game draw
function love.draw()


    -- Set drawing colour to white [default]
    g.setColor(255, 255, 255)

    -- Clear the game canvas
    gameCanvas:clear()

    -- Set the canvas to gameCanvas
    g.setCanvas(gameCanvas)

        -- Draw current state
        state.current:draw()

        g.setColor(255, 255, 255)

        if tolerance then

            g.draw(gfx.lBarEmpty, 40, 40)

            g.setScissor(40, 40, gfx.lBarFull:getWidth() * tolerance / 100, 71)

            g.draw(gfx.lBarFull, 40, 40)

            g.setScissor()

            g.setFont(fnt.timer)

            g.setColor(0, 0, 0)

            g.printf(string.format("%.1f",
                math.ceil(timer * 100) / 100) ..
                " seconds left" .. "\n Step " ..
                currentStep .. "/" .. #recipe.steps,
                1004, 44, 880, 'right')

            g.setColor(255, 255, 255)

            g.printf(string.format("%.1f",
                math.ceil(timer * 100) / 100) ..
                " seconds left" .. "\n Step " ..
                currentStep .. "/" .. #recipe.steps,
                1000, 40, 880, 'right')

            if feedback.timer > 0 then

                g.draw(gfx[feedback.type], 0, 0)

                if feedback.speech == "Chef" then

                    g.draw(gfx.speechChef, 40, 1080 - 193 - 40)

                else

                    g.draw(gfx.speechLlama, 1920 - 296, 1080 - 193 - 40)

                end

            end

        end

    -- Reset drawing canvas
    g.setCanvas()

    -- Find scale values
    local scaleW, scaleH =
        w.getWidth() / 1920,
        w.getHeight() / 1080

    -- Actually scale drawing
    g.scale(math.min(scaleW, scaleH))

    -- Translate drawing
    g.translate((math.max(scaleW / scaleH, 1) - 1) * 960,
        (math.max(scaleH / scaleW, 1) - 1) * 540)

    -- Actually draw canvas
    g.draw(gameCanvas, 0, 0)

    -- Reset drawing edits
    g.origin()


end


function love.keypressed(key)

    if key == "f11" and s.getOS() ~= "Android" then

        w.setFullscreen(not w.getFullscreen(), 'desktop')

    end

end


function sendFeedback(type)

    feedback = {
        timer = 1,
        speech = "",
        type = type
    }

    local num = math.random(21)

    if num <= 13 then

        sfx["chef" .. num]:play()

        feedback.speech = "Chef"

    else

        sfx["llama" .. (num - 13)]:play()

        feedback.speech = "Llama"

    end
end

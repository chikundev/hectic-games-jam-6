-- chikun :: 2014
-- Loads all sounds from the /bgm and /sfx folders


-- Recursively checks a folder for sounds
-- and adds any found to a table
function checkFolder(dir, tab)

    -- Determine sound type based on original dir
    if tab == nil then
        sndType = dir
        if dir == 'bgm' then
            stream = "stream"
        else
            stream = "static"
        end
    end

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = love.filesystem.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if love.filesystem.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            if val ~= "PLACEHOLDER" then

                -- Remove ".ogg" extension on file
                name = val:gsub(".ogg", "")

                -- Load sound into table
                tab[name] = love.audio.newSource(dir .. "/" .. val, stream)

                -- Set if looping based on if bgm
                tab[name]:setLooping(sndType == 'bgm')

            end

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

    end

    return tab

end


-- Load the bgm folder into the bgm table
bgm = checkFolder("bgm")


-- Load the sfx folder into the sfx table
sfx = checkFolder("sfx")


-- Kills checkFolder
checkFolder = nil

-- chikun :: 2015
-- Loads all fonts from the /fnt folder


-- Can't do this recursively due to sizes
fnt = {
    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 270),

    menuButton  = love.graphics.newFont("fnt/cabin.ttf", 72),

    timer  = love.graphics.newFont("fnt/cabin.ttf", 36)
}

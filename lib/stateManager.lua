-- chikun :: 2014
-- Manages different states


-- Create state table
state = {
    current = nil
}


-- Kill old state and load a new one
function state.change(newState)

    state.current:kill()
    state.load(newState)

end


-- Loads new state or reloads current state if argument omitted
function state.load(newState)

    state.old = state.current

    state.current = newState or state.current
    state.current:create()

end


-- Kill old state and sets another
function state.set(newState)

    state.old = state.current

    state.current.kill()
    state.current = newState

end


-- Update current state
function state.update(dt)

    state.current:update(dt)

end


-- Draw current state
function state.draw()

    state.current:draw()

end

-- chikun :: 2014
-- Miscellaneous functions



-- Convert resource from string to actual data
-- eg. 'gfx.shrek' to gfx.shrek's image data
-- Used by mapTool
function getResourceFromString(resource)

    -- Remove unneccessary parts of filename
    resource = resource:gsub(".jpg", "")
    resource = resource:gsub(".ogg", "")
    resource = resource:gsub(".png", "")
    resource = resource:gsub("%.%./", "")

    -- Replace slashes with periods to simplify loading
    resource = resource:gsub("/", "%.")

    -- Convert current string to table of strings
    tab = { }
    for word in resource:gmatch("([^.]+)") do
        tab[#tab + 1] = word
    end

    -- Recursively access deeper parts of table containing resource
    for key, value in ipairs(tab) do
        if key == 1 then
            resource = _G[value]
        else
            resource = resource[value]
        end
    end

    -- Return the found resource
    return(resource)

end



-- Does a deep copy of a table
-- Used by mapTool
function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return(copy)
end

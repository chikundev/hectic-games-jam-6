-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()


    tolerance = nil
    bgm.Track01:stop()

    failFade = 1

    backButton = {
        x = 40,
        y = 1040 - 112,
        w = 512,
        h = 112
    }

    retryButton = {
        x = 1920 - 40 - 512,
        y = 1040 - 112,
        w = 512,
        h = 112
    }

end


-- On state update
function newState:update(dt)

    failFade = math.max(failFade - dt, 0)

    if hasClicked then

        if math.overlap(curs, backButton) then

            state.change(states.mainMenu)

        elseif math.overlap(curs, retryButton) then

            state.change(states.play)

        end
    end
end


-- On state draw
function newState:draw()

    g.setColor(255, 255, 255)

    if failFade > 0 and false then

        g.setCanvas(mgCanvas)

        state.old:draw()

    end

    g.setCanvas(gameCanvas)

    g.setColor(255, 255, 255)

    local val = tostring(selectedRecipe)

    if val:len() == 1 then

        val = "0" .. val

    end

    g.draw(gfx.food["complete" .. val .. "Fail"], 0, 0)

    g.setFont(fnt.menuButton)

    g.printf("YOU ARE NOT A SKILLFUL LLAMA", 0, 80, 1920, 'center')

    g.setColor(255, 255, 255, failFade * 255)

    g.draw(mgCanvas, 0, 0)

    g.setColor(200, 140, 160)

    g.rectangle('fill', backButton.x, backButton.y, backButton.w, backButton.h)

    g.rectangle('fill', retryButton.x, retryButton.y, retryButton.w, retryButton.h)

    g.setColor(255, 255, 255)

    g.printf("Main Menu", backButton.x, backButton.y + 10, backButton.w, 'center')

    g.printf("Retry", retryButton.x, retryButton.y + 10, retryButton.w, 'center')

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState

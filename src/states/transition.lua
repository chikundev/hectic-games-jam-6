-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local tranState = { }


-- On state create
function tranState:create()

    if fadeMode == "out" then

        fade = 2
        fadeDir = -1

    else

        fade = 0
        fadeDir = 1

    end
end


-- On state update
function tranState:update(dt)

    fade = math.clamp(0, fade + fadeDir * dt * 1.5, 2)

    if (fadeDir == -1  and fade == 0) then

        state.load(states.mg[nextMG])

    elseif (fadeDir == 1 and fade == 2) then

        sendFeedback("yes")

        currentStep = currentStep + 1

        state.set(states.play)

        states.play:update(dt)

    end
end


-- On state draw
function tranState:draw()

    g.setColor(255, 255, 255)

    g.setCanvas(mgCanvas)

        states.mg[nextMG]:draw()

    g.setCanvas(gameCanvas)

    states.play:draw()

    local alpha = math.max(fade, 1) - 1
    local fader = math.min(fade, 1)

    g.setColor(255, 255, 255, 255 * (1 - alpha))

    g.draw(mgCanvas, 548 * fader, 40 * fader, 0,
        (1920 - 1096 * fader) / 1920, (1080 - 616 * fader) / 1080)

    g.setColor(255, 255, 255)

end


-- On state kill
function tranState:kill()

end


-- Transfer data to state loading script
return tranState

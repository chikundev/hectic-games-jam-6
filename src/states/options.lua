-- chikun :: 2015
-- Options state


-- Temporary state, removed at end of script
local optionsState = { }


-- On state create
function optionsState:create()

    bgmSelected, sfxSelected, androidMod =
        false,
        false,
        0

    if s.getOS() == "Android" then

        androidMod = 40

    end
end


-- On state update
function optionsState:update(dt)

    local bgmButton, sfxButton, checkbox, backButton =
        {
            x = 680 + (gfx.options.volume:getWidth() * vol.bgm) - 32,
            y = 400 + androidMod,
            w = 64,
            h = 72
        },
        {
            x = 680 + (gfx.options.volume:getWidth() * vol.sfx) - 32,
            y = 520 + androidMod,
            w = 64,
            h = 72
        },
        {
            x = 980,
            y = 640,
            w = 72,
            h = 72
        },
        {
            x = 400,
            y = 760,
            w = 1120,
            h = 40 + fnt.menuButton:getHeight()
        }

    if hasClicked then

        if math.overlap(curs, bgmButton) then

            bgmSelected = true

        elseif math.overlap(curs, sfxButton) then

            sfxSelected = true

        elseif math.overlap(curs, checkbox) then

            w.setFullscreen(not w.getFullscreen(), 'desktop')

        elseif math.overlap(curs, backButton) then

            state.set(states.mainMenu)

        end
    end

    if hasReleased then

        bgmSelected, sfxSelected =
            false,
            false

    end

    if bgmSelected then

        vol.bgm = math.clamp(0, (curs.x - 680) / gfx.options.volume:getWidth(), 1)

    elseif sfxSelected then

        vol.sfx = math.clamp(0, (curs.x - 680) / gfx.options.volume:getWidth(), 1)

    end
end


-- On state draw
function optionsState:draw()

    g.setColor(255, 255, 255)

    g.draw(gfx.mainMenu.bg, 0, 0)

    g.setFont(fnt.menuButton)

    g.printf("BGM", 460, 400 + androidMod, 160, 'right')
    g.draw(gfx.options.volume, 680, 400 + androidMod)
    g.draw(gfx.options.bubble,
        680 + (gfx.options.volume:getWidth() * vol.bgm) - 32, 400 + androidMod)

    g.printf("SFX", 460, 520 + androidMod, 160, 'right')
    g.draw(gfx.options.volume, 680, 520 + androidMod)
    g.draw(gfx.options.bubble,
        680 + (gfx.options.volume:getWidth() * vol.sfx) - 32, 520 + androidMod)

    if s.getOS ~= "Android" then

        g.printf("Fullscreen", 940, 640, 0, 'right')

        local checkbox = gfx.options.checkEmpty

        if w.getFullscreen() then

            checkbox = gfx.options.checkFull

        end

        g.draw(checkbox, 980, 640)

    end

    g.setColor(200, 140, 160)

    g.rectangle('fill', 400, 760, 1120, 40 + fnt.menuButton:getHeight())

    g.setColor(255, 255, 255)

    g.printf('Back', 0, 780, 1920, 'center')
end


-- On state kill
function optionsState:kill()

end


-- Transfer data to state loading script
return optionsState

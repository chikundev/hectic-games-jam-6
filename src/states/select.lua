-- chikun :: 2015
-- Select recipe state


-- Temporary state, removed at end of script
local selectState = { }


-- On state create
function selectState:create()

    bgm.Menu:play()

    selectedRecipe = 1

    backButton = {
        x = 40,
        y = 40,
        w = 304,
        h = 112
    }

end


-- On state update
function selectState:update(dt)

    if hasClicked then

        if math.overlap(backButton, curs) then

            state.set(states.mainMenu)

        end

        for key = 1, progress do

            local button = {
                x = 40 + (key - 1) % 3 * 606,
                y = 780,
                w = 506,
                h = 300
            }

            if math.overlap(button, curs) then

                selectedRecipe = key

            end
        end

        if math.dist(1600, 380, curs.x, curs.y) < 200 then

            bgm.Menu:stop()

            -- GO TO RECIPE GAME
            state.change(states.play)

        end
    end
end


-- On state draw
function selectState:draw()

    g.setFont(fnt.menuButton)

    g.setColor(255, 255, 255)

    g.draw(gfx.mainMenu.bg, 0, 0)

    local crep = recipes[selectedRecipe]

    g.setColor(0, 0, 0)

    g.printf(crep.name ..
        "\nTime: " .. crep.time .. " seconds" ..
        "\nSteps: " .. #crep.steps, 44, 244, 1200, 'center')

    g.setColor(255, 255, 255)

    g.printf(crep.name ..
        "\nTime: " .. crep.time .. " seconds" ..
        "\nSteps: " .. #crep.steps, 40, 240, 1200, 'center')

    g.setColor(100, 200, 240)

    g.circle('fill', 1600, 380, 200)

    g.setColor(255, 255, 255)

    g.printf("GO!", 1500, 344, 200, 'center')

    for key = 1, 3 do

        if key == selectedRecipe then

            g.setColor(100, 255, 60)

        elseif key <= progress then

            g.setColor(200, 140, 160)

        else

            g.setColor(180, 180, 180)

        end

        local x, y = 40 + (key - 1) % 3 * 606, 780

        g.rectangle('fill', x, y, 507, 260)

        g.setColor(255, 255, 255)

        g.printf("Recipe " .. key, x, y + 80, 507, 'center')

    end

    g.setColor(200, 140, 160)

    g.rectangle('fill', backButton.x, backButton.y, backButton.w, backButton.h)

    g.setColor(255, 255, 255)

    g.printf("Back", backButton.x, backButton.y + 10, backButton.w, 'center')

end


-- On state kill
function selectState:kill()

end


-- Transfer data to state loading script
return selectState

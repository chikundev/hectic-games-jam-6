-- chikun :: 2015
-- Template state


-- Temporary state, removed at end of script
local winState = { }


-- On state create
function winState:create()


    tolerance = nil

    bgm.Track01:stop()

    bgm.Track02:play()

    failFade = 1

    backButton = {
        x = 40,
        y = 1040 - 112,
        w = 512,
        h = 112
    }

    nextButton = {
        x = 1920 - 40 - 512,
        y = 1040 - 112,
        w = 512,
        h = 112
    }

end


-- On state update
function winState:update(dt)

    failFade = math.max(failFade - dt, 0)

    if hasClicked then

        if math.overlap(curs, backButton) then

            state.change(states.mainMenu)

        elseif math.overlap(curs, nextButton) then

            if selectedRecipe == #recipe.steps then

                state.change(states.credits)

            else

                selectedRecipe = selectedRecipe + 1

                if selectedRecipe > progress then

                    progress = selectedRecipe

                end

                state.change(states.play)

            end
        end
    end
end


-- On state draw
function winState:draw()

    g.setColor(255, 255, 255)

    if failFade > 0 and false then

        g.setCanvas(mgCanvas)

        state.old:draw()

    end

    g.setColor(255, 255, 255)

    g.setCanvas(gameCanvas)

    local val = tostring(selectedRecipe)

    if val:len() == 1 then

        val = "0" .. val

    end

    g.draw(gfx.food["complete" .. val], 0, 0)

    g.setFont(fnt.menuButton)

    g.printf("YOU HAVE PREPARED A DELICIOUS MEAL", 0, 80, 1920, 'center')

    g.setColor(200, 140, 160)

    g.rectangle('fill', backButton.x, backButton.y, backButton.w, backButton.h)

    g.rectangle('fill', nextButton.x, nextButton.y, nextButton.w, nextButton.h)

    g.setColor(255, 255, 255)

    g.printf("Main Menu", backButton.x, backButton.y + 10, backButton.w, 'center')



    if selectedRecipe == #recipe.steps then

        g.printf("Credits", nextButton.x, nextButton.y + 10, nextButton.w, 'center')

    else

        g.printf("Next Meal", nextButton.x, nextButton.y + 10, nextButton.w, 'center')

    end

end


-- On state kill
function winState:kill()

    bgm.Track02:stop()

end


-- Transfer data to state loading script
return winState

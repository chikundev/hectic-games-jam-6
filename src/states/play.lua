-- chikun :: 2015
-- Play state


-- Temporary state, removed at end of script
local playState = { }


-- On state create
function playState:create()

    bgm.Track01:play()


    map.setCurrent(recipes[selectedRecipe].map)

    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if (layer.name == "workspaces") then

            tools = layer.objects

        elseif (layer.name == "background") then

            layer.objects = { }

        end
    end

    recipe = recipes[selectedRecipe]

    tolerance = 50

    countdown = 3.99

    currentStep = 1

    timer = recipe.time

    ingredients = { }

    local top, bottom, inc =
        math.ceil(#recipe.ingredients / 2),
        math.floor(#recipe.ingredients / 2),
        1

    local topSeg, bottomSeg =
        1120 / top,
        1120 / bottom

    while inc <= top do

        ingImage = gfx.ingredients[recipe.ingredients[inc]]

        table.insert(ingredients,
            {
                x = (topSeg / 2) + topSeg * (inc - 1) - ingImage:getWidth() / 2,
                y = 750 - ingImage:getHeight() / 2,
                w = ingImage:getWidth(),
                h = ingImage:getHeight(),
                name = recipe.ingredients[inc]
            }
        )

        inc = inc + 1

    end

    while inc <= (top + bottom) do

        ingImage = gfx.ingredients[recipe.ingredients[inc]]

        table.insert(ingredients,
            {
                x = (bottomSeg / 2) + bottomSeg * (inc - top - 1) - ingImage:getWidth() / 2,
                y = 870 - ingImage:getHeight() / 2,
                w = ingImage:getWidth(),
                h = ingImage:getHeight(),
                name = recipe.ingredients[inc]
            }
        )

        inc = inc + 1

    end
end


-- On state update
function playState:update(dt)

    countdown = math.max(countdown - dt, 0)

    if countdown > 0 then return 0 end

    if lossFade then

        lossFade = math.max(lossFade - dt, 0)

        if lossFade <= 0 then

            state.change(states.lose)

        end

        return 0

    end

    if hasClicked then

        for key, value in ipairs(ingredients) do

            if math.overlap(curs, value) then

                selected = {
                    ingredient = value.name,
                    offsetX = curs.x - value.x,
                    offsetY = curs.y - value.y
                }

            end
        end
    end

    if hasReleased then

        for key, tool in ipairs(tools) do

            if math.overlap(curs, tool) and selected then

                if selected.ingredient == recipe.steps[currentStep].ingredient and
               tool.name == recipe.steps[currentStep].tool then

                    for key, ing in ipairs(ingredients) do

                        if ing.name == selected.ingredient then

                            table.remove(ingredients, key)

                        end
                    end

                    tolerance = tolerance + 10

                    if recipe.steps[currentStep].game then
                        states.mg[recipe.steps[currentStep].game]:create()
                        nextMG = recipe.steps[currentStep].game

                        fadeMode = "out"

                        state.load(states.transition)

                    else
                        currentStep = currentStep + 1

                        sendFeedback("yes")

                    end

                else

                    tolerance = tolerance - 5

                    sendFeedback("no")
                end
            end
        end

        selected = nil
    end

    if currentStep > #recipe.steps then

        progress = math.max(selectedRecipe + 1, progress)

        progress = math.min(progress, 10)

        state.change(states.win)

    end
end


-- On state draw
function playState:draw()

    g.setColor(255, 255, 255)

    g.draw(gfx.bg01, 0, 0)

    local chefCurrent, llamaCurrent =
        gfx.chef.happy,
        gfx.llama.happy

    if tolerance < 25 then

        chefCurrent = gfx.chef.angry

        llamaCurrent = gfx.llama.sad

    elseif tolerance < 75 then

        chefCurrent = gfx.chef.neutral

    end

    g.draw(chefCurrent, 40, 40, 0, 0.8, 0.8)

    g.draw(llamaCurrent, 1489, 40, 0, 0.8, 0.8)

    g.draw(gfx.counter, 0, 663)

    map.draw()

    for key, ingredient in ipairs(ingredients) do

        local tmpX, tmpY = ingredient.x, ingredient.y

        if selected then

            if selected.ingredient == ingredient.name then

                tmpX = curs.x - selected.offsetX
                tmpY = curs.y - selected.offsetY

            end
        end

        g.draw(gfx.ingredients[ingredient.name], tmpX, tmpY)

    end

    g.setFont(fnt.menuButton)

    if countdown > 1 then

        local img, scale =
            gfx["countdown" .. math.floor(countdown)],
            math.min(1, (1 - (countdown % 1)) * 2)

        g.draw(img, 960, 540, math.max((countdown % 1) - 0.5, 0) * 6, scale, scale,
            img:getWidth() / 2, img:getHeight() / 2)

    elseif countdown > 0 then

        local img, scale =
            gfx["countdownGo"],
            math.min(1, (1 - (countdown % 1)) * 2)

        g.draw(img, 960, 540, math.max((countdown % 1) - 0.5, 0) * 6, scale, scale,
            img:getWidth() / 2, img:getHeight() / 2)
    end
end


-- On state kill
function playState:kill()

    lossFade = nil
    tolerance = nil

end


-- Transfer data to state loading script
return playState

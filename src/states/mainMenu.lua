-- chikun :: 2014
-- Main menu state


-- Temporary state, removed at end of script
local mainMenuState = { }


-- On state create
function mainMenuState:create()

    bgm.Menu:play()

    sfx.chefHoo:play()

    mmVars = {
        fadeIn = 0,
        buttons = {
            "Select Mission",
            "Options",
            "Quit Game"
        }
    }
end


-- On state update
function mainMenuState:update(dt)

    mmVars.fadeIn = math.min(mmVars.fadeIn + dt, 1)

    if mmVars.fadeIn == 1 then

        if hasClicked then

            for key, button in ipairs(mmVars.buttons) do

                -- Temporary button
                local btn = {
                    x = 400,
                    y = 540 + (key - 1) * 150,
                    w = 1120,
                    h = 40 + fnt.menuButton:getHeight()
                }

                if math.overlap(btn, curs) then

                    if key == 1 then

                        -- Select mission screen
                        state.load(states.select)

                    elseif key == 2 then

                        -- Options screen
                        state.load(states.options)

                    elseif key == 3 then

                        e.quit()

                    end
                end
            end
        end
    end
end


-- On state draw
function mainMenuState:draw()

    local alpha = mmVars.fadeIn * 255

    g.setColor(255, 255, 255, alpha)

    g.draw(gfx.mainMenu.bg, 0, 0)

    g.setFont(fnt.menuButton)

    for key, button in ipairs(mmVars.buttons) do

        g.setColor(200, 140, 160, alpha)

        g.rectangle('fill', 400, 540 + (key - 1) * 150, 1120,
            40 + fnt.menuButton:getHeight())

        g.setColor(255, 255, 255, alpha)

        g.printf(button, 0, 560 + (key - 1) * 150, 1920, 'center')

    end

    g.draw(gfx.mainMenu.logo, 960 - gfx.mainMenu.logo:getWidth() / 2, 96)

end


-- On state kill
function mainMenuState:kill()

end


-- Transfer data to state loading script
return mainMenuState

-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local tomatoMGState = { }


-- On state create
function tomatoMGState:create()

    tImg = gfx.food.tomato1

    lImg = gfx.food.tomato1Line

    step = 1

    pointA = {}

    pointB = {}

end


-- On state update
function tomatoMGState:update(dt)

    if hasClicked then

        pointA = {
            x = curs.x,
            y = curs.y
        }

    end

    if hasReleased then

        if not pointA.x then

            return 0

        end

        pointB = {
            x = curs.x,
            y = curs.y
        }

        local xDiff, yDiff =
            (pointB.x - pointA.x) / 4,
            (pointB.y - pointA.y) / 4

        for i = 0, 5 do

            if math.dist(960, 540, pointA.x + i * xDiff, pointA.y + i * yDiff) < 270 then

                nextTomato()

                break

            end
        end

        pointA = { }
        pointB = { }

    end
end


-- On state draw
function tomatoMGState:draw()

    g.setColor(255, 255, 255)

    g.draw(gfx.bgBoard, 0, 0)

    g.draw(tImg, 960 - tImg:getWidth() / 2, 540 - tImg:getHeight() / 2)

    if step < 3 then

        g.draw(lImg, 960 - lImg:getWidth() / 2, 540 - lImg:getHeight() / 2)

    end

    if pointA.x then

        g.setColor(0, 0, 0)

        g.setLineWidth(8)

        g.line(pointA.x, pointA.y, curs.x, curs.y)

        g.setLineWidth(1)

    end

end


-- On state kill
function tomatoMGState:kill()

end


function nextTomato()

    if step == 1 then

        tImg = gfx.food.tomato2

    elseif step == 2 then

        tImg = gfx.food.tomato3

        tolerance = tolerance + 10

        sendFeedback("yes")

        nextMG = "stack"

        state.change(states.mg.stack)

    end

    step = step + 1

end


-- Transfer data to state loading script
return tomatoMGState

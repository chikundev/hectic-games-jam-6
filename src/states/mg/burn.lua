-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local burnMGState = { }


-- On state create
function burnMGState:create()

    stateIng = recipe.steps[currentStep].ingredient

    flipper = 0

    foodState = "Raw"

    offsetX = 0

    shortWait = 0

    blip = {
        x = 10000,
        y = 160,
        r = 122
    }

end


-- On state update
function burnMGState:update(dt)

    flipper = (flipper + dt * 4) % 2

    if shortWait > 0 then

        shortWait = shortWait - dt

        if shortWait <= 0 then

            if foodState == "Burnt" then

                state.change(states.mg.burn)

                sendFeedback("no")

            else

                fadeMode = "in"

                state.change(states.transition)

            end

        end

        return 0

    end

    if offsetX < -10000 then

        shortWait = 1

        foodState = "Burnt"

        tolerance = tolerance - 5

    elseif math.dist(curs.x, curs.y, blip.x, blip.y)<blip.r and hasClicked then

        shortWait = 1

        foodState = "Done"

        tolerance = tolerance + 10

    end

    offsetX = offsetX - 2000 * dt

    blip.x = offsetX + 8000

end


-- On state draw
function burnMGState:draw()

    g.setColor(255, 255, 255)

    g.draw(gfx.bgBacon, 0, 0)

    local scalHeight = 244 / gfx.countdown3:getHeight()

    g.draw(gfx.countdown3, 2000 + offsetX, 40, 0, scalHeight, scalHeight,
        gfx.countdown3:getWidth() / 2, 0)

    g.draw(gfx.countdown2, 4000 + offsetX, 40, 0, scalHeight, scalHeight,
        gfx.countdown3:getWidth() / 2, 0)

    g.draw(gfx.countdown1, 6000 + offsetX, 40, 0, scalHeight, scalHeight,
        gfx.countdown3:getWidth() / 2, 0)

    g.draw(gfx.clickIcon, blip.x, blip.y, 0, 1, 1,
        gfx.clickIcon:getWidth() / 2, gfx.clickIcon:getHeight() / 2)

    g.draw(gfx.food[stateIng .. "Pan" ..
        math.floor(flipper + 1)], 0, 0)

    g.draw(gfx.food[stateIng .. foodState], 680, 588)

    if foodState == "Burnt" then

        g.draw(gfx.food["eggsSmoke" ..
            math.floor(flipper + 1)], 0, 0)

    end
end


-- On state kill
function burnMGState:kill()

end


-- Transfer data to state loading script
return burnMGState

-- chikun :: 2015
-- Flip


-- Temporary state, removed at end of script
local flipMGState = { }


-- On state create
function flipMGState:create()

    toggle = nil

    flipMap = deepcopy(maps.patty)

    -- This is how we interpret map data
    for key, layer in ipairs(flipMap.layers) do

        if (layer.name == "patty") then

            patty = layer.objects[1]

            patty.image = gfx.food.pattyRaw

            table.remove(layer.objects, 1)

        elseif (layer.name == "pan") then

            pan = layer.objects[1]

        end
    end

    pattyFlying = 0
end


-- On state update
function flipMGState:update(dt)


    if pattyFlying == 0 and toggle then

        patty.x = pan.x + 140
        patty.y = pan.y + 147

        patty.image = gfx.food.pattyDone

        foodState = "Done"

        tolerance = tolerance + 10

        fadeMode = "in"

        state.change(states.transition)

        return 0

    end

    patty.y = patty.y + (pattyFlying * dt * 1900)

    if toggle then

        pan.x = curs.x - pan.w / 2
        pan.y = curs.y - pan.h / 2

    end

    local panBox = {
        x = pan.x + 86,
        y = pan.y + 189,
        w = 310,
        h = 25
    }

    if hasClicked then

        if pattyFlying == 0 and math.overlap(curs, pan) then

            pattyFlying = -1

            toggle = true

        end
    end

    if patty.y < -1280 and pattyFlying == -1 then
        patty.x = 100 + math.random(1520)

        pattyFlying = 1

    end

    if patty.y > 1080 then

        sendFeedback("no")

        state.change(states.mg.flip)

    elseif pattyFlying == 1 and math.overlap(patty, panBox) then

        pattyFlying = 0

    end
end


-- On state draw
function flipMGState:draw()

    g.setColor(255, 255, 255)

    map.draw(flipMap)

    g.draw(patty.image, patty.x, patty.y)

end


-- On state kill
function flipMGState:kill()

end


-- Transfer data to state loading script
return flipMGState

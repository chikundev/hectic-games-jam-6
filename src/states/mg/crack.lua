-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local crackMGState = { }


-- On state create
function crackMGState:create()

    stateIng = recipe.steps[currentStep].ingredient

    cracked = false
    crackedTimer = 0

    egg = {
        x = 741,
        y = 100,
        w = 437,
        h = 397
    }

end


-- On state update
function crackMGState:update(dt)

    if cracked then

        crackedTimer = math.min(crackedTimer + dt, 1)

        if crackedTimer == 1 then

            nextMG = "burn"

            state.change(states.mg.burn)

        end
    else

        if hasClicked and math.overlap(curs, egg) then

            cracked = true

            tolerance = tolerance + 10

            sendFeedback("yes")
        end

    end
end


-- On state draw
function crackMGState:draw()

    g.setColor(255, 255, 255)

    g.draw(gfx.bg02, 0, 0)

    if cracked then

        g.draw(gfx.food.eggBowlFull, 0, 0)

        g.draw(gfx.food.eggCracked, egg.x, egg.y)

    else

        g.draw(gfx.food.eggBowlEmpty, 0, 0)

        g.draw(gfx.food.eggUncracked, egg.x, egg.y)

    end

end


-- On state kill
function crackMGState:kill()

end


-- Transfer data to state loading script
return crackMGState

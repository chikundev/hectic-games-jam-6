-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local stackMGState = { }


-- On state create
function stackMGState:create()

    burgerBox = {
        x = 1038,
        y = 249,
        w = 842,
        h = 582
    }

    items = {
        {
            gx = burgerBox.x + 159,
            gy = burgerBox.y + 236,
            image = gfx.food.burgerPatty
        },
        {
            gx = burgerBox.x + 113,
            gy = burgerBox.y + 179,
            image = gfx.food.burgerCheese
        },
        {
            gx = burgerBox.x + 125,
            gy = burgerBox.y + 81,
            image = gfx.food.burgerLettuce
        },
        {
            gx = burgerBox.x + 190,
            gy = burgerBox.y + 190,
            image = gfx.food.burgerTomato
        }
    }

    for key, value in ipairs(items) do

        value.x = 480 - value.image:getWidth() / 2
        value.y = 1080 - (216 * key) - value.image:getHeight() / 2

        value.w = value.image:getWidth()
        value.h = value.image:getHeight()

    end


    heldItem = { }

end


-- On state update
function stackMGState:update(dt)

    if hasClicked then

        for key, value in ipairs(items) do

            if math.overlap(curs, value) and not value.placed then

                held = key

            end
        end

    elseif hasReleased then

        if curs.x > 1000 and held then

            items[held].x = items[held].gx
            items[held].y = items[held].gy

            items[held].placed = true

            held = nil
        end
    end

    for key, value in ipairs(items) do

        if not value.placed then

            return 0

        end
    end

    tolerance = tolerance + 10

    fadeMode = "in"

    state.change(states.transition)
end


-- On state draw
function stackMGState:draw()

    g.setColor(255, 255, 255)

    g.draw(gfx.bgBurger, 0, 0)

    g.draw(gfx.food.burgerBase, burgerBox.x, burgerBox.y + 152)

    for key, value in ipairs(items) do

        local tmpX, tmpY =
            value.x,
            value.y

        if key == held then

            tmpX = curs.x - value.image:getWidth() / 2
            tmpY = curs.y - value.image:getHeight() / 2

        end

        g.draw(value.image, tmpX, tmpY)

    end

    g.draw(gfx.food.burgerTop, burgerBox.x + 129, burgerBox.y)

end


-- On state kill
function stackMGState:kill()

end


-- Transfer data to state loading script
return stackMGState

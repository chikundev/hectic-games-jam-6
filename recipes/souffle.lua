-- chikun :: 2015
-- Souffle

return {

    name = "Chocolate Souffle",
    time = 120,
    ingredients = {
        "chocolate"
    },
    map = maps.game,
    steps = {
        {
            ingredient = "oil",
            tool = "stove",
            text = "Poured Oil"
        }
    }
}

-- chikun :: 2015
-- Bacon and eggs

return {

    name = "Bacon and eggs",
    time = 30,
    ingredients = {
        "bacon",
        "eggs",
        "oil"
    },
    map = maps.game,
    steps = {
        {
            ingredient = "oil",
            tool = "stove",
            text = "Poured Oil"
        },
        {
            ingredient = "bacon",
            tool = "stove",
            text = "Cook Bacon",
            game = "burn"
        },
        {
            ingredient = "eggs",
            tool = "stove",
            text = "Cook Eggs",
            game = "crack"
        }
    }
}

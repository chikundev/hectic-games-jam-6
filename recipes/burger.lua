-- chikun :: 2015
-- Cheeseburger

return {

    name = "Cheeseburger",
    time = 40,
    ingredients = {
        "mince",
        "cheese",
        "tomatoes",
        "lettuce",
        "oil"
    },
    map = maps.gameHB,
    steps = {
        {
            ingredient = "oil",
            tool = "stove",
            text = "Poured Oil"
        },
        {
            ingredient = "mince",
            tool = "stove",
            text = "Cooked Mince",
            game = "flip"
        },
        {
            ingredient = "tomatoes",
            tool = "board",
            text = "Cut Tomatoes",
            game = "tomato"
        }
    }
}

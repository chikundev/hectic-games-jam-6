-- chikun :: 2015
-- Vertical ash particle effect


local vertAsh = { }


function vertAsh:apply(grid, speed)

    for key, value in ipairs(grid) do

        value.dir = math.random(360)

    end

    grid.timer  = 0
    grid.speed  = speed

end


function vertAsh:update(grid, dt)

    -- Increase grid timer
    grid.timer = grid.timer + (dt * grid.speed)

    for key, value in ipairs(grid) do

        if value.id < grid.timer then

            value.y = value.y - (dt * grid.speed / 4)

            value.dir = (value.dir + dt * 120) % 360

            value.x = value.x + math.cos(math.rad(value.dir)) * dt * 64

            value.alpha = value.alpha - dt * 1.6

            value.colour = { math.random(255), math.random(255), math.random(255) }

        end

        if value.alpha <= 0 then

            table.remove(grid, key)

        end

    end

end


return vertAsh
